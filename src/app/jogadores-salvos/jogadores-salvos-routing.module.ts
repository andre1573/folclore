import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JogadoresSalvosPage } from './jogadores-salvos.page';

const routes: Routes = [
  {
    path: '',
    component: JogadoresSalvosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JogadoresSalvosPageRoutingModule {}
