import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { Jogador } from '../models/jogador';

@Component({
  selector: 'app-jogadores-salvos',
  templateUrl: './jogadores-salvos.page.html',
  styleUrls: ['./jogadores-salvos.page.scss'],
})
export class JogadoresSalvosPage extends AppUtil implements OnInit {

  jogadoresSalvos: Array<Jogador>;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
  }

  ngOnInit() {
  }

  public incluir(){
    super.backPage();
  }

}
