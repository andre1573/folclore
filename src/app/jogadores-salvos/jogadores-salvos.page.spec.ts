import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JogadoresSalvosPage } from './jogadores-salvos.page';

describe('JogadoresSalvosPage', () => {
  let component: JogadoresSalvosPage;
  let fixture: ComponentFixture<JogadoresSalvosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JogadoresSalvosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JogadoresSalvosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
