import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JogadoresSalvosPageRoutingModule } from './jogadores-salvos-routing.module';

import { JogadoresSalvosPage } from './jogadores-salvos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JogadoresSalvosPageRoutingModule
  ],
  declarations: [JogadoresSalvosPage]
})
export class JogadoresSalvosPageModule {}
