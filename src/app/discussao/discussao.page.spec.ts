import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DiscussaoPage } from './discussao.page';

describe('DiscussaoPage', () => {
  let component: DiscussaoPage;
  let fixture: ComponentFixture<DiscussaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscussaoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DiscussaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
