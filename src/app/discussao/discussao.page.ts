import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { AppComponent } from '../app.component';
import { ControlePartida } from '../models/controle-partida';

@Component({
  selector: 'app-discussao',
  templateUrl: './discussao.page.html',
  styleUrls: ['./discussao.page.scss'],
})
export class DiscussaoPage extends AppUtil implements OnInit {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
  }

  ngOnInit() {
  }

  public novoJogo(){
    this.soundClick.play();
    super.openPage('jogadores');
  }

}
