export class Papel {

    constructor(key: string, nome: string) {
        
        this.key = key;
        this.nome = nome;
        this.qtd = 0;
        this.imagem = key;
        this.isMacho = false;
        this.qtdSkills = 9999;
        if(key == "cacador") {
            this.isMaligno = true;
            this.descricao = "O Caçador consegue matar qualquer criatura alvejada, desde que o alvo não esteja imune ou capturada. (Os caçadores podem ver quem são os outros caçadores através da bandeira de aliado)";
            this.isAssassino = true;
            this.isMacho = true;
        }else{
            this.isMaligno = false;
            if(key == "lobisomem"){
                this.descricao = "A mordida do Lobisomem tem 50% de chance de matar o alvo.";
                this.isInfectado = true;
                this.isMacho = true;
            }else if(key == "curupira"){
                this.descricao = "O Curupira é o guardião da floresta, ele pode atacar qualquer um, mas tenha cuidado com os inocentes. (Máximo de 2 usos)";
                this.isAssassino = true;
                this.isMacho = true;
                this.qtdSkills = 2;
            }else if(key == "iara"){
                this.descricao = "A Iara é uma linda sereia que seduz os homens. Os alvos seduzidos não podem matá-la. (Máximo de 1 uso)";
                this.isSeduz = true;
                this.qtdSkills = 1;
            }else if(key == "boi-tata"){
                this.descricao = "O Boitatá pode proteger um personagem qualquer à sua escolha por uma noite.";
                this.isProtege = true;
                this.isMacho = true;
            }else if(key == "saci"){
                this.descricao = "O alvo do Saci-pererê não poderá matar ou morrer durante a noite. (Máximo de 1 uso)";
                this.isCaptura = true;
                this.isMacho = true;
                this.qtdSkills = 1;
            }else if(key == "cuca"){
                this.descricao = "A Cuca pode usar sua bola de cristal para ver o personagem de algum jogador. (Máximo de 1 uso)";
                this.isVidente = true;
                this.qtdSkills = 1;
            }else if(key == "corpo-seco"){
                this.descricao = "O Corpo-Seco é um morto-vivo que não possui nenhuma habilidade.";
                this.isMacho = true;
            }
        }
    }

    //configs
    public imagem: string;
    public key: string;
    public nome: string;
    public qtd: number;
    public isMaligno: boolean;
    public descricao: string;

    //habilidades
    public isAssassino: boolean; //caçador e curupira
    public isInfectado: boolean; //lobisomem
    public isSeduz: boolean; //iara
    public isProtege: boolean; //boi-tata
    public isCaptura: boolean; //saci
    //passivas
    public isImune: boolean; //nao pode morrer: corpo-seco
    public isVidente: boolean; //cuca
    public isMacho: boolean; //macho/homem
    public qtdSkills: number; //qtd máxima do uso de skills por game

    public maisQtd(){
        this.qtd++;
    }

    public menosQtd(){
        this.qtd--;
    }
}