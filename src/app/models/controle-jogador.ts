import { Jogador } from './jogador';
import { Papel } from './papel';
import { Controle } from './controle';

export class ControleJogador {

    constructor(jogador: Jogador, id: number) {
      this.jogador = jogador;
      this.isMorto = false;
      this.isPreso = false;
      this.qtdDiasPreso = 0;
      this.votosRecebidos = 0;
      this.isPronto = false;
      this.id = id;
      this.isJaFezSuaAcao = false;
      this.isForaDoJogo = false;
      this.colorSelected = "border: solid #ff0000;";
    }

    public id: number;
    public jogador: Jogador;
    public isForaDoJogo: boolean;
    //modifica/reseta por dia
    public colorSelected: string;
    public isPronto: boolean; //clique para mostrar suas ações/papel
    public votosRecebidos: number;
    public isJaFezSuaAcao: boolean;
    public idSeuAssassino: number; //id do jogador que matou ele
     //consequencias
     public isPreso: boolean;
     public qtdDiasPreso: number;
     public isMorto: boolean;
     public isSeduzido: boolean;
     public isProtegido: boolean;
     public isCapturado: boolean;
}