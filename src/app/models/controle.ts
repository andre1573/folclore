import { Jogador } from './jogador';
import { Papel } from './papel';

export class Controle {

    constructor() {
       this.qtdNoites = 0;
       this.jogadores = new Array<Jogador>();
    }

    public jogadores: Array<Jogador>;
    public todosPapeis: Array<Papel>;
    public vivos: Array<Jogador>;
    public mortos: Array<Jogador>;
    public qtdNoites: Number;
}