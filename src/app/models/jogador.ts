import { Papel } from './papel';

export class Jogador {

    constructor(nome: string, qtdSkills?: number) {
        this.nome = nome;
        this.qtdSkills = qtdSkills;
        this.imagem = "./../assets/imgs/fotoicone.png";
    }
    

    public nome: string;
    public papel: Papel;
    public imagem: string;    
    public qtdSkills: number; //qtd máxima do uso de skills por game
    
}