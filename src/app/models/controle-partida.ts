import { Jogador } from './jogador';
import { Papel } from './papel';
import { Controle } from './controle';
import { ControleJogador } from './controle-jogador';

export class ControlePartida {

    constructor(jogadoresControle: Array<ControleJogador>, papeisEmJogo: Array<Papel>) {
      this.jogadoresControle = jogadoresControle;
      this.papeisEmJogo = papeisEmJogo;
      this.msgEndGame = "";
      this.numDoDia = 0;
      this.isNoite = true;
      this.mortosDaNoite = new Array<Jogador>();
      this.indexJogadorAtual = 0;
      this.isInGame = false;
      this.bgImgPath = "./../assets/imgs/bgnoite.jpg";
      this.keyBgFim = "";
    }

    jogadoresControle: Array<ControleJogador>;
    papeisEmJogo: Array<Papel>;
    isInGame: boolean;
    msgEndGame: string;
    keyBgFim: string;

    //modifica/reseta por dia
    jogadorAtual: ControleJogador;
    indexJogadorAtual: number;
    numDoDia: number;
    isNoite: boolean;
    bgImgPath: string;
    mortosDaNoite: Array<Jogador>;
    alvoSelecionado: ControleJogador;
    idMaisVotado: number;

 
}