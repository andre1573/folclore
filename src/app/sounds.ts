
import { ControleJogador } from './models/controle-jogador';
import { NavController, AlertController} from '@ionic/angular';
import { Controle } from './models/controle';
import { AppComponent } from './app.component';
import { Jogador } from './models/jogador';

export class Sounds {

    sound: any;
    play: any;
    stop: any;

    constructor(src: string) {
        this.sound = document.createElement("audio");
        this.sound.src = src;
        this.sound.setAttribute("preload", "auto");
        this.sound.setAttribute("controls", "none");
        this.sound.style.display = "none";
        document.body.appendChild(this.sound);
        this.play = function(){
          this.sound.play();
        }
        this.stop = function(){
          this.sound.pause();
        }
      }


}



