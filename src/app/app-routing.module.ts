import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'jogadores',
    loadChildren: () => import('./jogadores/jogadores.module').then( m => m.JogadoresPageModule)
  },
  {
    path: 'regras',
    loadChildren: () => import('./regras/regras.module').then( m => m.RegrasPageModule)
  },
  {
    path: 'papeis',
    loadChildren: () => import('./papeis/papeis.module').then( m => m.PapeisPageModule)
  },
  {
    path: 'jogo',
    loadChildren: () => import('./jogo/jogo.module').then( m => m.JogoPageModule)
  },
  {
    path: 'adicionar-jogador',
    loadChildren: () => import('./adicionar-jogador/adicionar-jogador.module').then( m => m.AdicionarJogadorPageModule)
  },
  {
    path: 'jogadores-salvos',
    loadChildren: () => import('./jogadores-salvos/jogadores-salvos.module').then( m => m.JogadoresSalvosPageModule)
  },
  {
    path: 'endgame',
    loadChildren: () => import('./endgame/endgame.module').then( m => m.EndgamePageModule)
  },  {
    path: 'discussao',
    loadChildren: () => import('./discussao/discussao.module').then( m => m.DiscussaoPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
