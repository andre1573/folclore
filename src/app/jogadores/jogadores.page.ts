import { Component, OnInit } from '@angular/core';
import { Jogador } from '../models/jogador';
import { AppUtil } from '../app-util';
import { NavController, AlertController} from '@ionic/angular';
import { AppComponent } from '../app.component';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-jogadores',
  templateUrl: './jogadores.page.html',
  styleUrls: ['./jogadores.page.scss'],
})
export class JogadoresPage extends AppUtil implements OnInit {

  public players: Array<Jogador>;

  constructor(public navCtrl: NavController, private nativeStorage: NativeStorage, public alertCtrl: AlertController) {
    super(navCtrl,alertCtrl);
  }

  ngOnInit() {
    this.players = AppComponent.controle.jogadores;
    console.log("OS jogadores ngoninit:: ");
    console.log(this.players);
  }

  public adicionarJogador(){ 
    this.soundClick.play();
    super.openPage('adicionar-jogador');
  }

  public removerJogador(jogadorR: Jogador){
    var newJogadores = new Array<Jogador>();
    for (var jogador of  AppComponent.controle.jogadores){
      if(jogador!=jogadorR){
        newJogadores.push(jogador);
      }else{
        console.log("IGUAAAAAAAAAAAAAAL");
      }
    }
    AppComponent.controle.jogadores = newJogadores;
    this.players = newJogadores;
  }

  public jogadoresSalvos(){
    this.soundClick.play();
    super.openPage('jogadores-salvos');
  }

  public limparJogadores(){
    this.nativeStorage.setItem('jogadores', '')
    .then(
      () => console.log('Limpando lista de jgoadores na memoria'),
      error => console.error('Error storing item', error)
    );
    AppComponent.controle.jogadores = new Array<Jogador>();
    this.players = AppComponent.controle.jogadores;
  }

  public configurarPapeis(){
    if(this.players.length < 3){
      this.soundClickError.play();
      super.mostrarAlert("Atenção", "É necessário no mínimo 3 jogadores para começar o jogo.");
      return;
    }else{
      this.soundClick.play();
      super.openPage('papeis');
    } 
  }

}
