import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JogoPage } from './jogo.page';

describe('JogoPage', () => {
  let component: JogoPage;
  let fixture: ComponentFixture<JogoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JogoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JogoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
