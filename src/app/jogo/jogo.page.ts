
import { AppComponent } from './../app.component';
import { ControleJogador } from './../models/controle-jogador';
import { Component, OnInit } from '@angular/core';
import { ControlePartida } from '../models/controle-partida';
import { Papel } from '../models/papel';
import { Jogador } from '../models/jogador';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-jogo',
  templateUrl: './jogo.page.html',
  styleUrls: ['./jogo.page.scss'],
})
export class JogoPage extends AppUtil implements OnInit {

  public selectedColor: string = 'border: solid #ff0000;'; 

  jogadorAtual: ControleJogador;
  controlePartida: ControlePartida;
  alvos: Array<ControleJogador>;
  //
  mensagemInicio = "";
  mensagemResumoNoite = "";
  mensagemResumoDia = "";
  mensagemAlvo = "";
  discussao = false;

  isIniciouNoite: boolean;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
    super.preencherSounds();
  }

  ngOnInit() {
    this.preencherControlePartida();
    this.mensagemInicio = "Entregue o aparelho ao narrador do jogo (" + this.controlePartida.jogadoresControle[1].jogador.nome + ")...\n"+ 
    "A noite se aproxima, todos devem esperar sua vez de receber o celular para efetuar sua jogada.";
  }

  public iniciarJogo(){
    super.iniciarJogo();
    this.ngOnInit();
  }

  private preencherControlePartida(){
    if(!AppComponent.ControlePartida.isInGame){ //entra aqui apenas na primeira vez qdo starta o game
      this.isIniciouNoite = true;
      this.alvos = new Array<ControleJogador>();
      let controlesJogador = new Array<ControleJogador>();
      let papeisEmJogo = new Array<Papel>();
      controlesJogador.push(this.criarMestre()); //Adiciona o mestre na lista de jogadores
      let id=1;
      for (var jogador of  AppComponent.controle.jogadores) { //adiciona os jogadores na lista e preenche a lista de papeis em jogo
        jogador.qtdSkills = jogador.papel.qtdSkills;
        controlesJogador.push(new ControleJogador(jogador, id));
        papeisEmJogo.push(jogador.papel);
        id++;
      }
      AppComponent.ControlePartida = new ControlePartida(controlesJogador, papeisEmJogo);
      AppComponent.ControlePartida.isInGame = true;
      AppComponent.ControlePartida.indexJogadorAtual = 0;
      AppComponent.ControlePartida.jogadorAtual = controlesJogador[0];
      this.controlePartida = AppComponent.ControlePartida;
      this.mensagemAlvo = "Selecione um alvo: ";
      console.log( AppComponent.ControlePartida);
    }else{ //entra aqui a cada mudança de jogador
      AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = false;
      this.jogadorAtual = AppComponent.ControlePartida.jogadorAtual;
      this.controlePartida.alvoSelecionado = null;
      if(this.jogadorAtual.jogador.papel.key == "mestre" && !this.controlePartida.isNoite){ //amanheceu
        this.mensagemAlvo = "Vote no suspeito para ser assassinado pela aldeia.";
        super.aplicarAcoesDaNoite();
        this.mensagemResumoNoite = super.getMensagemResumoNoite();
        this.discussao = true;
        this.isIniciouNoite = false;
        //this.startTimer(1);
        console.log("Resumo noite: "+this.mensagemResumoNoite);
      }else if(this.jogadorAtual.jogador.papel.key == "mestre" && this.controlePartida.isNoite){ //anoiteceu
        this.mensagemAlvo = "Selecione um alvo: ";
        super.aplicarAcoesDoDia();
        this.mensagemResumoDia = super.getMensagemResumoDia();
        console.log("Resumo dia: "+this.mensagemResumoDia);
      }
      this.preencherAtributosAposTurno();
      console.log(this.jogadorAtual);
    }
  }

  private preencherAtributosAposTurno(){
    this.controlePartida = AppComponent.ControlePartida;
    this.alvos = new Array<ControleJogador>();
    for (var jogadorC of  AppComponent.ControlePartida.jogadoresControle) { 
      if(this.isAlvoValido(jogadorC)){
        this.alvos.push(jogadorC);
      }
    }
  }

  public isImobilizado(jogador: ControleJogador){
    if(jogador.isPreso || jogador.isCapturado ){
      return true;
    }else{
      return false;
    }
  }

  public iniciarEleicao(){
    this.iniciarNovaRodada();
  }

  public iniciarNoite(){
    this.isIniciouNoite = true;
    this.playSoundAnoitecer();
    this.iniciarNovaRodada();
  }

  public iniciarNovaRodada(){ //ação do mestre/narrador
    super.iniciarNovaRodada();
    this.ngOnInit();
  }

  public finalizarAcaoJogador(){ //acao dos jogadores
    super.finalizarAcaoJogador();
    this.ngOnInit();
  }

  private isAlvoValido(jogadorC: ControleJogador): boolean{ //dependendo da acao do jogador, pode nao poder alvejar alguem
    if(jogadorC.id == 0 || jogadorC.id == this.jogadorAtual.id || jogadorC.isForaDoJogo ){ //não pode alvejar o mestre ou ele mesmo
      return false;
    }else{
      return true;
    }
  }

  public OnSelectAlvo(alvo: ControleJogador){
    if(alvo.isPreso){
      super.mostrarAlert("Atenção", "O alvo selecionado está preso e não pode ser alvejado.");
      return;
    }
    this.selectedColor = "border: solid #f30000;"; //desired Color
    this.controlePartida.alvoSelecionado = alvo;
    console.log("Alvo selecionado: "+alvo.id+" e "+this.controlePartida.alvoSelecionado.id);
  }

  private criarMestre(): ControleJogador{ //cria o narrador/mestre
    let mestreJogador = new Jogador("Mestre");
    let papelMestre = new Papel("mestre", "Mestre");
    mestreJogador.imagem = AppComponent.controle.jogadores[0].imagem;
    mestreJogador.papel = papelMestre;
    mestreJogador.qtdSkills = papelMestre.qtdSkills;
    let mestreControle = new ControleJogador(mestreJogador, 0);
    mestreControle.isMorto = false;
    this.jogadorAtual = mestreControle;
    return mestreControle;
  }

  public verPapel(){
    this.jogadorAtual.isPronto = true;
  }

  //Habilitar botoes //rendereds
  public habilitarBtnAssassinar(): boolean{
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isMaligno){
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnAtacar(): boolean{
    if(!this.controlePartida.isNoite  || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isAssassino && !this.jogadorAtual.jogador.papel.isMaligno){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnMorder(): boolean{
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isInfectado){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnPrever(){
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isVidente){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnSeduzir(){
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isSeduz){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnProteger(){
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isProtege){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  public habilitarBtnCapturar(){
    if(!this.controlePartida.isNoite || this.controlePartida.jogadorAtual.isJaFezSuaAcao) return false;
    if(this.jogadorAtual.jogador.papel.isCaptura){
      if(this.jogadorAtual.jogador.qtdSkills<=0) return false;
      return true;
    }else{
      return false;
    }
  }
  //fim habilitar botoes


  //botoes ações
  public acaoVotar(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar alguém para votar!");
      return;
    }
    super.acaoVotar(this.controlePartida.alvoSelecionado.id);
    this.finalizarAcaoJogador();
  }

  public acaoPassar(){
    this.finalizarAcaoJogador();
  }

  public acaoDormir(){
    this.finalizarAcaoJogador();
  }

  public preparaAssassinar(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Assassinando o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.preparaAssassinar(this.controlePartida.alvoSelecionado.id);
    this.finalizarAcaoJogador();
  }
  public acaoAtacar(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Atacando o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoAtacar(this.controlePartida.alvoSelecionado.id);
    this.finalizarAcaoJogador();
  }
  public acaoMorder(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Mordendo o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoMorder(this.controlePartida.alvoSelecionado.id); //FALTA IMPLEMENTAR
    this.finalizarAcaoJogador();
  }
  public async acaoPrever(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Vendo papel do jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoPrever(this.controlePartida.alvoSelecionado.id); 
    //this.finalizarAcaoJogador();
  }
  public acaoSeduzir(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Seduzindo o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoSeduzir(this.controlePartida.alvoSelecionado.id); //FALTA IMPLEMENTAR
    this.finalizarAcaoJogador();
  }
  public acaoProteger(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Protegendo o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoProteger(this.controlePartida.alvoSelecionado.id);
    this.finalizarAcaoJogador();
  }
  public acaoCapturar(){
    if(this.controlePartida.alvoSelecionado === null){
      super.mostrarAlert("Atenção", "Você precisa selecionar um alvo!");
      return;
    }
    console.log("Capturando o jogador "+this.controlePartida.alvoSelecionado.jogador.nome);
    super.acaoCapturar(this.controlePartida.alvoSelecionado.id);
    this.finalizarAcaoJogador();
  }
  //fim botoes ações

  time: BehaviorSubject<string> = new BehaviorSubject('00:30');
  timer: number;

  public startTimer(duration: number){
    this.timer = duration * 30; //alterar para 60 depois
    setInterval(() => {
      this.updateTimeValue();
    }, 1000);
  }

  public updateTimeValue() {
    let minutes: any = this.timer / 60;
    let seconds: any = this.timer % 60;

    minutes = String('0' + Math.floor(minutes)).slice(-2);
    seconds = String('0' + Math.floor(seconds)).slice(-2);

    const text = minutes + ':' + seconds;
    this.time.next(text);

    --this.timer;

    if(this.timer < 0) {
      this.time = new BehaviorSubject('00:30');
      this.timer = 0
      this.discussao = false;
    }

  }

}
