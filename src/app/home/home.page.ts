import { Sounds } from './../sounds';
import { Component } from '@angular/core';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { AppComponent } from '../app.component';
import { Controle } from '../models/controle';
import { Papel } from '../models/papel';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
//import { transition } from '@angular/core/src/animation/dsl'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage extends AppUtil {

  constructor(public navCtrl: NavController, private nativeStorage: NativeStorage, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl); 
  }

  ngOnInit() {
    AppComponent.controle = new Controle(); 
    this.pegarJogadoresDaMemoria();
  }

  public novoJogo(){
    //AppComponent.controle = new Controle(); 
    //this.limparJogadores(); //REMOVER ISSO DEPOIS, SO SERVE PRA TESTAR NO DEVAPP
    //this.pegarJogadoresDaMemoria();
    this.soundClick.play();
    super.openPage('jogadores');
  }

  public comoJogar(){
    this.soundClick.play();
    super.openPage('discussao');
  }

  public limparJogadores(){
    this.nativeStorage.setItem('jogadores', '')
    .then(
      () => console.log('Limpando lista de jgoadores na memoria'),
      error => console.error('Error storing item', error)
    );
  }

  private pegarJogadoresDaMemoria(){
    this.nativeStorage.getItem('jogadores')
      .then(
        data => {
          console.log("data: ");
          console.log(data);
          if(data!=null){
            console.log("encontrouuuuuuuu na memoria os jogadores"+data);
            AppComponent.controle.jogadores = JSON.parse(data);
            console.log("Apos preencher: ");
            console.log(AppComponent.controle.jogadores);
          }
        },
        error => {
          console.log("Deu erro:");
          console.error(error);
        }
    );
  }


}
