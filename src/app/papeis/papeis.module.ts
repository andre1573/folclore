import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PapeisPageRoutingModule } from './papeis-routing.module';

import { PapeisPage } from './papeis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PapeisPageRoutingModule
  ],
  declarations: [PapeisPage]
})
export class PapeisPageModule {}
