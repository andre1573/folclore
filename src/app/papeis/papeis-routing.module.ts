import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PapeisPage } from './papeis.page';

const routes: Routes = [
  {
    path: '',
    component: PapeisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PapeisPageRoutingModule {}
