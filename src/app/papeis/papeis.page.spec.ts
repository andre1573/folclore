import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PapeisPage } from './papeis.page';

describe('PapeisPage', () => {
  let component: PapeisPage;
  let fixture: ComponentFixture<PapeisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PapeisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PapeisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
