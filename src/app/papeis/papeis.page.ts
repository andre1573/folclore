import { AppComponent } from './../app.component';
import { ControlePartida } from './../models/controle-partida';
import { Controle } from './../models/controle';
import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { Papel } from '../models/papel';

@Component({
  selector: 'app-papeis',
  templateUrl: './papeis.page.html',
  styleUrls: ['./papeis.page.scss'],
})
export class PapeisPage extends AppUtil implements OnInit {

  public papeis: Array<Papel>;
  public papeisEmJogo: Array<Papel>;
  public qtdJogadores: number;
  public qtdPapeis: number;
  public random: boolean = true;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
  }

  ngOnInit() {
    AppComponent.controle.todosPapeis = this.carregarPapeis();
    this.qtdJogadores = AppComponent.controle.jogadores.length;
    this.papeis = AppComponent.controle.todosPapeis;
    this.papeisEmJogo = new Array<Papel>();
    this.qtdPapeis = 0;
  }

  private carregarPapeis(): Array<Papel>{
    let papeis = new Array<Papel>();
   
    let cacador = new Papel("cacador", "Caçador");
    papeis.push(cacador);
    let saci = new Papel("saci", "Saci-Pererê");
    papeis.push(saci);
    let cuca = new Papel("cuca", "Cuca");
    papeis.push(cuca);
    let curupira = new Papel("curupira", "Curupira");
    papeis.push(curupira);
    let iara = new Papel("iara", "Iara");
    papeis.push(iara);
    let corpoSeco = new Papel("corpo-seco", "Corpo-Seco");
    papeis.push(corpoSeco);
    let boiTata = new Papel("boi-tata", "Boitatá");
    papeis.push(boiTata);
    let lobisomem = new Papel("lobisomem", "Lobisomem");
    papeis.push(lobisomem);
    
    return papeis;
  }

  public maisQtdPapeis(){
    this.qtdPapeis++;
  }

  public menosQtdPapeis(){
      this.qtdPapeis--;
  }

  public comecar(){
    if(this.random){
      this.preencherPapeisRandom();
      this.sortearPapeis();
      this.avancar();
    }else{
      if(this.qtdPapeis != this.qtdJogadores){
        super.mostrarAlert("Atenção", "É necessário adicionar mais tipos de papéis em jogo para continuar");
      } else{
        if(this.preencherPapeisEmJogo()){
          this.sortearPapeis();
          this.avancar();
        }
      }
    }
  }

  public avancar(){
    AppComponent.ControlePartida = new ControlePartida(null, null);
    this.soundClick.play();
    super.openPage('jogo');
  }

  private preencherPapeisEmJogo(): boolean{
    this.papeisEmJogo = []; 
    let isPossuiAssassino = false;
    for(let i=0;i<this.papeis.length;i++){
      for(let j=0;j<this.papeis[i].qtd;j++){
        this.papeisEmJogo.push(this.papeis[i]);
        if(this.papeis[i].isAssassino){
          isPossuiAssassino = true;
        }
      }
    }
    if(!isPossuiAssassino){
      super.mostrarAlert("Atenção", "É preciso no mínimo 1 Caçador para começar o jogo!");
      return false;
    }else{
      return true;
    }
  }

  private sortearPapeis(){
    console.log(this.papeisEmJogo);
    console.log("embaralhando...");
    this.papeisEmJogo = this.embaralhar(this.papeisEmJogo);
    console.log(this.papeisEmJogo);
    for(let i=0;i<this.papeisEmJogo.length;i++){
      AppComponent.controle.jogadores[i].papel = this.papeisEmJogo[i];
    }
  }

  private preencherPapeisRandom(){ 
    this.papeisEmJogo = []; 
    let qtdHunter = 1;
    let maxHunter = 1;
    if(this.qtdJogadores > 6) maxHunter=2;

    this.papeisEmJogo.push(this.pegarPapelCacador());
    for(let i=0;i<this.qtdJogadores-1;i++){
      let ok: boolean = false;
      while(!ok){
        let papel: Papel = this.pegarPapelRandom();
        if(papel.key!="cacador"){
          this.papeisEmJogo.push(papel);
          ok = true;
        }else if(qtdHunter < maxHunter){
          this.papeisEmJogo.push(papel);
          qtdHunter++;
          ok = true;
        }
      }
    }
  }

  private pegarPapelCacador(): Papel{
    for(let i=0;i<AppComponent.controle.todosPapeis.length;i++){
      if(AppComponent.controle.todosPapeis[i].key=="cacador") return AppComponent.controle.todosPapeis[i];
    }
  }

  private pegarPapelRandom(): Papel{
    AppComponent.controle.todosPapeis = this.embaralhar(AppComponent.controle.todosPapeis);
    return AppComponent.controle.todosPapeis[0];
  }

  private embaralhar(array: Array<Papel>): Array<Papel> {
    return array.sort(() => Math.random() - 0.5);
  }

  public changePapeisRandomAtivo(event){
    this.random = !this.random;
  }

}
