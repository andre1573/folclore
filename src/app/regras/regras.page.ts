import { Component, OnInit } from '@angular/core';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { AppComponent } from '../app.component';
import { ControlePartida } from '../models/controle-partida';

@Component({
  selector: 'app-regras',
  templateUrl: './regras.page.html',
  styleUrls: ['./regras.page.scss'],
})
export class RegrasPage extends AppUtil implements OnInit {

  textoComoJogar: string;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
  }

  ngOnInit() {
   this.textoComoJogar = "";
  }

  public iniciarJogo(){
    AppComponent.ControlePartida = new ControlePartida(null, null);
    super.openPage('jogo');
  }

}
