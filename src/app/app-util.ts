import { ControleJogador } from './models/controle-jogador';
import { NavController, AlertController} from '@ionic/angular';
import { Controle } from './models/controle';
import { AppComponent } from './app.component';
import { Jogador } from './models/jogador';
import { Sounds } from './sounds';

export class AppUtil {

  private mensagemResumoDaNoite: string = "";
  private mensagemResumoDoDia: string = "";

  soundAmanhecer: any;
  soundAnoitecer: any;
  soundClick: any;
  soundClickError: any;
  soundCacadoresWin: any;
  soundCriaturasWin: any;
  soundEmpate: any;

  private assassinatos: Array<{jogadorAtual: ControleJogador, alvo: ControleJogador, alvoId: number}> = [];

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
    this.preencherSounds();
  }

  public openPage(pageName: string){
     this.navCtrl.navigateForward('/'+pageName);
 }
  public openPageRoot(pageName: string){
    this.navCtrl.navigateRoot('/'+pageName);
  }

  public atualizarPagina(){
    window.location.reload();
  }

  public backPage(){
    this.navCtrl.back();
  }

  public iniciarJogo(){
    AppComponent.ControlePartida.isNoite = true;
    AppComponent.ControlePartida.numDoDia = 1;
    AppComponent.ControlePartida.indexJogadorAtual = 0;
    AppComponent.ControlePartida.mortosDaNoite = new Array<Jogador>();
    this.playSoundAnoitecer();
    this.proximoJogador();
  }

  public preencherSounds(){
    this.soundAmanhecer = new Sounds("../assets/sounds/amanhecendo.wav");
    this.soundAnoitecer = new Sounds("../assets/sounds/anoitecendo.wav");
    this.soundClick = new Sounds("../assets/sounds/click.wav");
    this.soundClickError = new Sounds("../assets/sounds/clickerror.wav");
    this.soundCacadoresWin = new Sounds("../assets/sounds/cacadoreswin.wav");
    this.soundCriaturasWin = new Sounds("../assets/sounds/criaturaswin.wav");
    this.soundEmpate = new Sounds("../assets/sounds/empate.wav");
  }

  public iniciarNovaRodada(){ //Ação do mestre
    if(AppComponent.ControlePartida.isNoite){
      AppComponent.ControlePartida.numDoDia++;
    }else{
      //this.playSoundAnoitecer();
    }
  
    this.proximoJogador();
    AppComponent.ControlePartida.mortosDaNoite = new Array<Jogador>();
  }

  public finalizarAcaoJogador(){
    AppComponent.ControlePartida.jogadoresControle[AppComponent.ControlePartida.indexJogadorAtual].isPronto = false;
    this.proximoJogador();
  }

  private amanhecer(){
    AppComponent.ControlePartida.indexJogadorAtual = 0;
    AppComponent.ControlePartida.isNoite = false;
    this.playSoundAmanhecer();
  }

  private anoitecer(){
    AppComponent.ControlePartida.indexJogadorAtual = 0;
    AppComponent.ControlePartida.isNoite = true;
  }

  private proximoJogador(){
    AppComponent.ControlePartida.indexJogadorAtual++;
    if(AppComponent.ControlePartida.indexJogadorAtual > AppComponent.ControlePartida.jogadoresControle.length-1){
      AppComponent.ControlePartida.indexJogadorAtual = 0;
    }
    while(AppComponent.ControlePartida.jogadoresControle[AppComponent.ControlePartida.indexJogadorAtual].isForaDoJogo){
      AppComponent.ControlePartida.indexJogadorAtual++;
      if( AppComponent.ControlePartida.indexJogadorAtual > AppComponent.ControlePartida.jogadoresControle.length-1){
        break;
      }  
    }
    console.log("Atual: "+AppComponent.ControlePartida.indexJogadorAtual);
    console.log("Lenght: "+AppComponent.ControlePartida.jogadoresControle.length);
    if(AppComponent.ControlePartida.indexJogadorAtual > AppComponent.ControlePartida.jogadoresControle.length-1 || AppComponent.ControlePartida.indexJogadorAtual == 0){ //todos os jogadores terminaram de jogar. Volta pro narrador = index 0
      AppComponent.ControlePartida.indexJogadorAtual = 0;
      AppComponent.ControlePartida.jogadorAtual = AppComponent.ControlePartida.jogadoresControle[0];
      if(AppComponent.ControlePartida.isNoite){
        this.amanhecer();
      }else{
        this.anoitecer();
      } 
    }else{
      AppComponent.ControlePartida.jogadorAtual = AppComponent.ControlePartida.jogadoresControle[AppComponent.ControlePartida.indexJogadorAtual];
    }
  }
  
  private getRandomArbitrary() {
    return Math.floor(Math.random() * 10 + 1)
  }

  //botoes ações
  public acaoVotar(alvoId: number){
    console.log("Votando");
    AppComponent.ControlePartida.jogadoresControle[alvoId].votosRecebidos++;
  }

  public preparaAssassinar(alvoId: number){
    this.assassinatos.push({jogadorAtual: AppComponent.ControlePartida.jogadorAtual, alvo: AppComponent.ControlePartida.alvoSelecionado, alvoId: alvoId})
    console.log(this.assassinatos)

    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
  }

  private acaoAssassinar(jogadorAtual: ControleJogador, alvo: ControleJogador, alvoId: number){
    console.log("Assassinando");
    if(this.isPodeAssassinar(jogadorAtual, alvo)){ //condicoes onde não eh possivel aplicar a ação
      AppComponent.ControlePartida.jogadoresControle[alvoId].isMorto = true;
      AppComponent.ControlePartida.jogadoresControle[alvoId].idSeuAssassino = jogadorAtual.id;
     }    
  }

  public acaoAtacar(alvoId: number){
    console.log("Mordendo");
    if(this.isPodeAssassinar(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
      console.log("matou o alvo com um ataque: ");
      AppComponent.ControlePartida.jogadoresControle[alvoId].isMorto = true;
      AppComponent.ControlePartida.jogadoresControle[alvoId].idSeuAssassino = AppComponent.ControlePartida.jogadorAtual.id;
    }
    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }

  public acaoMorder(alvoId: number){
    console.log("Mordendo");
    if(this.isPodeMorder(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
      //if(AppComponent.ControlePartida.alvoSelecionado.jogador.papel.isMaligno && !AppComponent.ControlePartida.alvoSelecionado.jogador.papel.isInfectado)
      if(this.getRandomArbitrary()%2==0){ //50% chance de matar
        console.log("matou o alvo com uma mordida: ");
        AppComponent.ControlePartida.jogadoresControle[alvoId].isMorto = true;
        AppComponent.ControlePartida.jogadoresControle[alvoId].idSeuAssassino = AppComponent.ControlePartida.jogadorAtual.id;
      }else{ //TRASNFORMAR NAO IMPLEMENTADO
       // console.log("transformou o alvo em lobisomem");
       // AppComponent.ControlePartida.jogadoresControle[alvoId].jogador.papel.isInfectado = true;
      }
    }
    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }
  public acaoPrever(alvoId: number){
    console.log("Descobrindo papel do jogador");
    if(this.isPodePrever(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
       this.mostrarAlert("Clarividência", "O alvo selecionado foi: '"+ AppComponent.ControlePartida.alvoSelecionado.jogador.papel.nome+"'");
    }else{
       this.mostrarAlert("Clarividência", "O feitiço falhou, não foi possível encontrar o alvo.");
    }
    //AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }
  public acaoSeduzir(alvoId: number){
    console.log("Seduzindo o jogador");
    if(this.isPodeSeduzir(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
      AppComponent.ControlePartida.jogadoresControle[alvoId].isSeduzido = true;
    }
    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }
  public acaoProteger(alvoId: number){
    console.log("Protegendo o jogador");
    if(this.isPodeProteger(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
      AppComponent.ControlePartida.jogadoresControle[alvoId].isProtegido = true;
    }
    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }
  public acaoCapturar(alvoId: number){
    console.log("Capturando o jogador");
    if(this.isPodeCapturar(AppComponent.ControlePartida.jogadorAtual, AppComponent.ControlePartida.alvoSelecionado)){ //condicoes onde não eh possivel aplicar a ação
      AppComponent.ControlePartida.jogadoresControle[alvoId].isCapturado = true;
      console.log("capturooou o jogador "+alvoId+"  > "+ AppComponent.ControlePartida.jogadoresControle[alvoId].jogador.nome);
    }
    AppComponent.ControlePartida.jogadorAtual.isJaFezSuaAcao = true;
    AppComponent.ControlePartida.jogadorAtual.jogador.qtdSkills--;
  }
  //fim botoes ações

  //verificacoes se pode executar acao
  private isPodeAssassinar(jogador: ControleJogador, alvo: ControleJogador): boolean{
    if(jogador.isSeduzido && alvo.jogador.papel.isSeduz) return false; //nao pode assasinar se a iara tiver seduzido ele
    if(alvo.jogador.papel.isImune || alvo.isProtegido || alvo.isCapturado || jogador.isCapturado || jogador.isPreso ){
      return false;
    }else{
      return true;
    }
  }

  private isPodeMorder(jogador: ControleJogador, alvo: ControleJogador): boolean{
    if(alvo.jogador.papel.isImune || alvo.isProtegido || alvo.isCapturado){
      return false;
    }else{
      return true;
    }
  }

  private isPodePrever(jogador: ControleJogador, alvo: ControleJogador): boolean{
    if(!alvo.isCapturado){
      return true;
    }else{
     return false; 
    }
  }

  private isPodeSeduzir(jogador: ControleJogador, alvo: ControleJogador): boolean{
    if(alvo.jogador.papel.isMacho && !alvo.isCapturado){ //somente machos
      return true;
    }else{
      return false;
    }
  }

  private isPodeProteger(jogador: ControleJogador, alvo: ControleJogador): boolean{
    if(!alvo.isCapturado && !alvo.isPreso){
      return true;
    }else{
     return false; 
    }
  }

  private isPodeCapturar(jogador: ControleJogador, alvo: ControleJogador): boolean{
    return true;
  }
  //fim verificacoes se pode executar acao

  public getMensagemResumoNoite(): string{
    return this.mensagemResumoDaNoite;
  }

  public getMensagemResumoDia(): string{
    return this.mensagemResumoDoDia;
  }

  private finalizarGame(){
    AppComponent.ControlePartida.msgEndGame += "Caçadores:";
    let first: boolean = true;
    let qtdMalignos: number = 0;
    for(var jogadorM of  AppComponent.ControlePartida.jogadoresControle){ //Revelar quem eram os caçadores
      if(jogadorM.jogador.papel.isMaligno && jogadorM.jogador.papel.key != "mestre"){
        if(!first) AppComponent.ControlePartida.msgEndGame += ", ";
        else AppComponent.ControlePartida.msgEndGame += " ";
        AppComponent.ControlePartida.msgEndGame += jogadorM.jogador.nome;
        first = false;
        qtdMalignos++;
      }
    }
   // if(qtdMalignos>1) AppComponent.ControlePartida.msgEndGame += " eram os caçadores.";
   // else AppComponent.ControlePartida.msgEndGame += " era o caçador.";
    
    this.openPage('endgame');
  }

  private isPossuiAldeoesVivos(): boolean{
    for(var jogadorM of  AppComponent.ControlePartida.jogadoresControle){ //se tiver mais lobisomem ou caçador o jogo continua, se todos tiverem morrido, o jogo acaba
      if(!jogadorM.jogador.papel.isMaligno && !jogadorM.isForaDoJogo && !jogadorM.jogador.papel.isImune && jogadorM.jogador.papel.key != "mestre"){
        return true;
      }
    }
    return false;
  }

  private isPossuiMalignosVivos(): boolean{
    for(var jogadorM of  AppComponent.ControlePartida.jogadoresControle){ //se tiver mais lobisomem ou caçador o jogo continua, se todos tiverem morrido, o jogo acaba
      if(jogadorM.jogador.papel.isMaligno && !jogadorM.isForaDoJogo && jogadorM.jogador.papel.key != "mestre"){
        return true;
      }
    }
    return false;
  }

  private zerarAtributosPosNoite(id: number){
    AppComponent.ControlePartida.jogadoresControle[id].isCapturado = false;
    AppComponent.ControlePartida.jogadoresControle[id].isJaFezSuaAcao = false;
    AppComponent.ControlePartida.jogadoresControle[id].isMorto = false;
    AppComponent.ControlePartida.jogadoresControle[id].isPreso = false;
    AppComponent.ControlePartida.jogadoresControle[id].isProtegido = false;
    AppComponent.ControlePartida.jogadoresControle[id].isSeduzido = false;
    AppComponent.ControlePartida.jogadoresControle[id].idSeuAssassino = null;
  }

  private zerarAtributosPosDia(id: number){
    AppComponent.ControlePartida.jogadoresControle[id].votosRecebidos = 0;
  }

  public aplicarAcoesDoDia(){ //terminou o dia e vai anoitecer
    console.log("entrou em acoes do dia");
    this.mensagemResumoDoDia = "";
    let idMaisVotado = 0 ;
    let maiorVoto = -1;
    let qtdMaisVotados = 0;
    let totalVotos = 0;
    let totalVivos = 0;
    for (var jogadorC of  AppComponent.ControlePartida.jogadoresControle) { 
      if(jogadorC.jogador.papel.key != "mestre"){
        totalVotos += jogadorC.votosRecebidos;
        totalVivos++;
        if(jogadorC.votosRecebidos > maiorVoto){
          idMaisVotado = jogadorC.id;
          maiorVoto = jogadorC.votosRecebidos;
          qtdMaisVotados=1;
        }else if(jogadorC.votosRecebidos === maiorVoto){
          qtdMaisVotados++;
        }
        this.zerarAtributosPosDia(jogadorC.id);
      }
    }
    let qtdNaoVotantes = totalVivos-totalVotos;
    AppComponent.ControlePartida.idMaisVotado = idMaisVotado;
    this.assassinarJogadorMaisVotado(idMaisVotado, maiorVoto, qtdMaisVotados, qtdNaoVotantes);

    if(!this.isPossuiMalignosVivos() && this.isPossuiAldeoesVivos()){ 
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgcriaturaswin";
      this.finalizarGame();
    } else if(!this.isPossuiAldeoesVivos() && this.isPossuiMalignosVivos()){
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgcacadoreswin";
      this.finalizarGame();
    }else if(!this.isPossuiAldeoesVivos() && !this.isPossuiMalignosVivos()){
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgempate";
      this.finalizarGame();
    }
   
    console.log("finalizou acoes do dia");
  }

  public aplicarAcoesDaNoite(){ //terminou a noite e vai amanhecer
    console.log("entrou em acoes da noite");
    this.mensagemResumoDaNoite = "";
    let txJogadoresMortos = "";
    let houveMortes = false;
    let first = true;
    let qtdMortos = 0;

    for (var assassinato of this.assassinatos){
      console.log(assassinato)
      this.acaoAssassinar(assassinato.jogadorAtual, assassinato.alvo, assassinato.alvoId)
    }
    
    for (var jogadorC of  AppComponent.ControlePartida.jogadoresControle) { 
      if(jogadorC.jogador.papel.key!="mestre" && !jogadorC.isForaDoJogo){
        if(jogadorC.isMorto && !jogadorC.isProtegido && !jogadorC.isCapturado && !this.verificarSeSeuAssassinoEstaCapturado(jogadorC.idSeuAssassino)){ //cerificar se o assassino do jogador atual foi capturado na noite pra n deixar amtar se o assassino tiver capturado
          houveMortes = true;
          jogadorC.isForaDoJogo = true;
          qtdMortos++;
          if(first){
            first = false;
            txJogadoresMortos += ""+jogadorC.jogador.nome + ""; 
          }else{
            txJogadoresMortos += ", "+jogadorC.jogador.nome + "";
          }
        }
        console.log(AppComponent.ControlePartida.msgEndGame);
        this.zerarAtributosPosNoite(jogadorC.id);
      }
    }

    if(qtdMortos>1) this.mensagemResumoDaNoite += ""+txJogadoresMortos+" foram encontrados mortos na floresta.";
    else this.mensagemResumoDaNoite += txJogadoresMortos+" foi encontrado morto na floresta.";

    console.log("aplicando acoes da noite");
    if(!this.isPossuiMalignosVivos() && this.isPossuiAldeoesVivos()){ 
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgcriaturaswin";
      this.finalizarGame();
    } else if(!this.isPossuiAldeoesVivos() && this.isPossuiMalignosVivos()){
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgcacadoreswin";
      this.finalizarGame();
    }else if(!this.isPossuiAldeoesVivos() && !this.isPossuiMalignosVivos()){
      AppComponent.ControlePartida.msgEndGame = "";
      AppComponent.ControlePartida.keyBgFim = "bgempate";
      this.finalizarGame();
    }

    if(!houveMortes){
      this.mensagemResumoDaNoite = "A floresta está em paz, não houve mortes essa noite.";
    }else{
      //this.mensagemResumoDaNoite += ". <br> Vítimas não podem falar até o fim do jogo.";
    }
  
    console.log("finalizou acoes da noite");
  }

  private isNaoPossuiMalignosOuSoPossuiMalignosVivos(){
    let qtdMalignos: number = 0;
    let qtdCriaturas: number = 0;
    for (var jogadorC of AppComponent.ControlePartida.jogadoresControle) { 
      if(jogadorC.jogador.papel.isMaligno && !jogadorC.isForaDoJogo && jogadorC.jogador.papel.key!="mestre"){
        qtdMalignos++;
      }
      if(!jogadorC.jogador.papel.isMaligno && !jogadorC.isForaDoJogo && jogadorC.jogador.papel.key!="mestre"){
        qtdCriaturas++;
      }
    }
    console.log("criaturas vivas: "+qtdCriaturas);
    console.log("caçadores vivos: "+qtdMalignos);
    if(qtdMalignos<=0&&qtdCriaturas<=0) return true;
    if(qtdMalignos<=0) return true;
    if(qtdCriaturas<=0) return true;
    if(qtdMalignos>0&&qtdCriaturas>0) return false;
    return false;
  }

  private verificarSeSeuAssassinoEstaCapturado(id: number){
    console.log("//seu assassino esta captuadaod?" +AppComponent.ControlePartida.jogadoresControle[id].isCapturado);
    console.log(AppComponent.ControlePartida.jogadoresControle[id]);
    if(id==null) return false;
    if(AppComponent.ControlePartida.jogadoresControle[id].isCapturado) return true;
    return false;
  }

  private assassinarJogadorMaisVotado(idMaisVotado: number, maiorVoto: number, qtdMaisVotados: number, qtdNaoVotantes: number){
    if(qtdNaoVotantes>maiorVoto) {
      this.mensagemResumoDoDia+= "Não houve votos suficientes, ninguém será assassinado pela aldeia."
    }else if(qtdMaisVotados>1){ //ninguem é morto
      this.mensagemResumoDoDia+= "Votação empatada, ninguém será assassinado pela aldeia.";
    }else if(qtdMaisVotados===0){
      this.mensagemResumoDoDia+= "Não houve votos suficientes, ninguém será assassinado pela aldeia.";
    }else{ //Assassinar
      AppComponent.ControlePartida.jogadoresControle[idMaisVotado].isMorto = true;
      AppComponent.ControlePartida.jogadoresControle[idMaisVotado].isForaDoJogo = true;
      this.mensagemResumoDoDia+= ""+ AppComponent.ControlePartida.jogadoresControle[idMaisVotado].jogador.nome+" foi assassinado(a) pela aldeia com "+maiorVoto+" votos. O jogador está forá do jogo.";
    }
  }
  
  public async mostrarAlert(titulo, texto) {
    const alert = await this.alertCtrl.create({
      header: titulo,
      message: texto,
      buttons: ['OK']
    });

    await alert.present();
  }

  public playSoundAnoitecer(){
    this.soundAnoitecer.play();
  }
  public playSoundAmanhecer(){
    this.soundAmanhecer.play();
  }

}
