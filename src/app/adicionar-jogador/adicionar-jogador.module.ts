import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdicionarJogadorPageRoutingModule } from './adicionar-jogador-routing.module';

import { AdicionarJogadorPage } from './adicionar-jogador.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdicionarJogadorPageRoutingModule
  ],
  declarations: [AdicionarJogadorPage]
})
export class AdicionarJogadorPageModule {}
