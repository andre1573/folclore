import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdicionarJogadorPage } from './adicionar-jogador.page';

const routes: Routes = [
  {
    path: '',
    component: AdicionarJogadorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdicionarJogadorPageRoutingModule {}
