import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { Jogador } from '../models/jogador';
import { AppUtil } from '../app-util';
import { NavController, AlertController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-adicionar-jogador',
  templateUrl: './adicionar-jogador.page.html',
  styleUrls: ['./adicionar-jogador.page.scss'],
})
export class AdicionarJogadorPage extends AppUtil implements OnInit {

  public jogador: Jogador;
  private tirouFoto: boolean;
 // public photo: string = '';

  constructor(public navCtrl: NavController, private nativeStorage: NativeStorage, private camera: Camera, public alertCtrl: AlertController) {
    super(navCtrl, alertCtrl);
  }

  ngOnInit() {
    this.jogador = new Jogador('');
    this.tirouFoto = false;
  }

  public takePicture() {
    this.jogador.imagem = '';
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 100,
      targetHeight: 100
    }
    this.camera.getPicture(options)
      .then((imageData) => {
        let base64image = 'data:image/jpeg;base64,' + imageData;
        console.log("nao deu erro pra tirar foto");
        this.jogador.imagem = base64image;
        this.tirouFoto = true;
      }, (error) => {
        this.jogador.imagem = "./../assets/imgs/fotoicone.png";
        console.error(error);
      })
      .catch((error) => {
        console.log("aaaao");
        this.jogador.imagem = "./../assets/imgs/fotoicone.png";
        console.error(error);
      })
      console.log(" foto"+this.jogador.imagem);
  }

  private salvarJogadoresStorage(jogadores: Array<Jogador>){
    this.nativeStorage.setItem('jogadores', JSON.stringify(jogadores))
    .then(
      () => console.log('Stored item! OK'),
      error => console.error('Error storing item', error)
    );
  }

  public salvar(){
    if(this.jogador.nome == null || this.jogador.nome == '' || this.jogador.nome == undefined){
      this.soundClickError.play();
      super.mostrarAlert("Atenção", "O campo nome precisa ser preenchido");
    }else{
      var newJogador = new Jogador(this.jogador.nome);
      if(!this.tirouFoto) this.jogador.imagem = "./../assets/imgs/player.png";
      newJogador.imagem = this.jogador.imagem; 
      AppComponent.controle.jogadores.push(newJogador);
      this.salvarJogadoresStorage(AppComponent.controle.jogadores);
      this.soundClick.play();
      super.backPage();
    }
  }

}
