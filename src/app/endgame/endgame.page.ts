import { AppComponent } from './../app.component';
import { ControlePartida } from './../models/controle-partida';
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController} from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AppUtil } from '../app-util';

@Component({
  selector: 'app-endgame',
  templateUrl: './endgame.page.html',
  styleUrls: ['./endgame.page.scss'],
})
export class EndgamePage extends AppUtil implements OnInit {

  msgEndGame: string;
  nomeBgFim: string; 

  constructor(public navCtrl: NavController, private nativeStorage: NativeStorage, public alertCtrl: AlertController) {
    super(navCtrl,alertCtrl);
  }

  ngOnInit() {
    this.nomeBgFim = AppComponent.ControlePartida.keyBgFim;
    this.msgEndGame = AppComponent.ControlePartida.msgEndGame;
    if(AppComponent.ControlePartida.keyBgFim=="bgempate") this.soundEmpate.play();
    if(AppComponent.ControlePartida.keyBgFim=="bgcriaturaswin") this.soundCriaturasWin.play();
    if(AppComponent.ControlePartida.keyBgFim=="bgcacadoreswin") this.soundCacadoresWin.play();
    console.log("NOME BG FIM: "+this.nomeBgFim);
  }

  public jogarNovamente(){
    super.openPage('papeis');
  }

}
